/*
	npm init -y  /  npm init
	npm install express
	npm install mongoose
	touch .gitignore >> content: mode_modules
*/


const express = require("express");
const mongoose = require("mongoose");

/*	
	models folder >> task.js
	contollers folder >> taskController.js
	route folder >> taskRoute.js
*/
const taskRoute = require("./routes/taskRoute.js");

// Server Setup
const app = express();
const port = 3001;

//Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// DB connection
mongoose.connect("mongodb+srv://admin:admin@b218-to-do.arttd8q.mongodb.net/toDo?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

app.use("/tasks", taskRoute);

app.listen(port, ()=> console.log(`Now listening to port ${port}`));

// GitBash:
	// nodemon index.js