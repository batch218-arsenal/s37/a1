// This document contains all the endpoints for our application (also http methods )

const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskController.js");


router.get("/viewTasks", (request, response) =>{
	// Invokes the "getAllTasks" function from the "taskController.js" file and sends the result back to the client/Postman
									//returns result from our controller
	taskController.getAllTask().then(result => response.send(result));
})

router.post("/addNewTask", (request, response) => {
	taskController.createTask(request.body).then(result => response.send(result));
})
							//:id will serve as our wildcard
router.delete("/deleteTask/:id", (request, response)=> {
	taskController.deleteTask(request.params.id).then(result => response.send(result));
})

router.put("/updateTask/:id", (request, response) => {
	// "id" will be the basis of what document we will update
	// request.body - the new documents/contents
	taskController.updateTask(request.params.id, request.body).then(result => response.send(result));
})

// [ACTIVITY - s37]

router.get("/:id", (request, response) =>{

	taskController.specificTask(request.params.id).then(result => response.send(result));
})

router.put("/:id/complete", (request, response) => {

	taskController.statusUpdate(request.params.id, request.body).then(result => response.send(result));
})



module.exports = router;
