// This document contains our app feature in displaying and manipulating our database

const Task = require("../models/task.js");

module.exports.getAllTask = () => {

	return Task.find({}).then(result => {
		return result;
	})
}

module.exports.createTask = (requestBody) => {

	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false;
		} else {
			return task;
		}
	})
}

// "taskId" parameter will serve as storage of id in our url/link
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, error) => {
		if(error){
			console.log(error);
			return false;
		} else {
			return removedTask;
		}
	})
}

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		} 

		result.name = newContent.name;

		return result.save().then((updateTask, saveError)=>{
			if(saveError){
				console.log(saveError);
				return false
			} else {
				return updateTask;
			}
		})
	})
}

// [ACTIVITY - s37]

module.exports.specificTask = (taskId) => {
	return Task.findById(taskId).then((result, error) => {

	if(error){
			console.log(error);
			return false;
		} else {
			return result;
		}

	})
} 

module.exports.statusUpdate = (taskId, newStatus) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		} 

		result.status = newStatus.status;	
		
		return result.save().then((updatedStatus, saveError)=>{
			if(saveError){
				console.log(saveError);
				return false
			} else {
				return updatedStatus;
			}
		})	
	})
}